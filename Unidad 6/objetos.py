# es un paradigma de programacion 
#un objeto es una abstraccion del mundo fisico 
#las caracteristicas que poseen un objeto son los atributos
#la acciones que puede realizar el objeto son metodos
#una clase es una plantilla o un molde del objeto
class Humano:
	
	#init python lo reconoce y se ejecuta de manera automatica
	def __init__(self,edad):
		#atributos
		self.edad = edad
		print "Soy un Humano"
	def hablar(self,mensaje):
		print mensaje

pedro=Humano(30)
pedro.hablar("Hola")
print "Hola soy pedro y tengo ", pedro.edad
		
