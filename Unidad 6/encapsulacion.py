#los atributos y los metodos tiene la propiedad de ser privados o publicos

# __ = privado

class Prueba:
	def __init__(self,):
		self.__privado="soy privado"
		self.publico="Soy publico"

	def __metodoPrvado(self):
		print "Soy privado"
	def metodoPublico(self):
		print "Soy publico"

	def getPrivado(self):
		return self.__privado
	def setPrivado(self,valor):
		self.__privado=valor


obj=Prueba()
print obj.publico
#print obj.__privado
obj.metodoPublico()
print obj.getPrivado()
