#son un tipo de coleccion ordenada se dice que son arrays
l=[2,"tres",True,["uno",10]]
print l
#indice al cual quiero ingresar
posicion=1
l2=l[posicion]
print l2

#accesar al elemento de una lista dentro de otra lista
l2=l[3][1]
print l2

#cambiando un elemento de la lista
l[1]=4
print l

#copiar una lista a otra 
l2=l[0:3]
print l2

#copiar objetos especificos
l2=l[0:3:2]
print l2

#copiar todos los elementos a partir del indice 0
l2=l[0::2]
print l2

#cambiar valores de la lista
l[1:3]=[1,2,3]
print l

#accesar a las lista pero al inverso
l2=l[-1]
print l2