#Comillas simples
cadenaS='Texto entre Comillas sencillas'

#utilizando caracteres de escape

cadenaS='Texto entre \tComillas sencillas'

#Comillas dobles
cadenaD= "Texto entre Comillas dobles"

#comillas triples
cadenaT="""Texto linea 1
linea 2
linea 3 
linea 4
"""



print type(cadenaS)
print cadenaS
print cadenaD
print cadenaT


#para pasar una variable cualquiera a un string
variable=4
str(variable)

#Concatenar cadena

cadenaC=cadenaS+cadenaD
print cadenaC

#para saber si el string contiene a otro

cadena1="qwertyuiop"
cadena2="er"
contenida=cadena2 in cadena1
print contenida

#Saber la longitud de una cadena
longitud=len(cadena1)
print longitud

#Repeticion
print cadena2*3

#Extraer una sub cadena 
subcadena=cadenaS[0:6]
print subcadena