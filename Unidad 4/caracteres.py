#Los caracteres strings de longitud 1, para extraer
# uno se utiliza cadena[posicion]

cadena="abcdefg"
caracter=cadena[0]

print caracter

caracter=cadena[1]
print caracter

caracter=cadena[-1]
print caracter

#para obtener el codigo asignado a un caracter
#se utiliza ord(caracter)

cadena=raw_input("Introduce una letra: ")
caracter=cadena[0]
numero=ord(caracter)
print "El caracter ", caracter , " es el numero ", numero

#para obtener un caracter a partir de su numero
#se utiliza chr(numero)

numero= input("Introduce un numero : ")
caracter=chr(numero)
print "el numero ", numero , "corresponde al caracter ", caracter