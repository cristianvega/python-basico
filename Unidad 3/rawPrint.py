nombre='joe'
i=10
# es igual a i=10

#Se puede reutilizar nombres de variables, ejemplo:

r=200
r='xyz'

#Se puede Hacer variables con el valor de otras

var=i

print "Hola", nombre
#para permitir al usuario introducir datos existen raw_input e input

#para cadenas de texto se usa rawinput 
nombre=raw_input("Como te llamas")
print "Hola\t", nombre

#para introducir numeros se usa input

numero = input("Introduce un numero: ")
print "El numero que ingreso es:\n",numero