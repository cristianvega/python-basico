#encoding: UTF -8
class Humano:
	def __init__(self,edad): #los guiones al piso indican el nivel de seguridad
		self.edad=edad 
		print "Hola soy un humano y tengo",edad,"años"
	def correr(self,km): #si los metodos no tienen la palabra reservada self se entiende que el metodo no pertenece a la clase
		print "Corro",km,"kilometros"
	def hablar(self,mensaje): #diferentes metodos pertenecientes a la clase Humano
		print "Les quiero decir que",mensaje

i=0 #for i=0
while i<=10: #for(i=0;i<=10)
	edad=input("Ingrese su edad")
	objpersona=Humano(edad)
	i+=1 #for(i=0;i<=10;i++) ciclo for en C++

pedro=Humano(30)#creacion de un objeto
pedro.correr(20)
pedro.hablar("Estoy hablando")

juan=Humano(15)
juan.correr(10)