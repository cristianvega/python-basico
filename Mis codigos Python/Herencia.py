#encoding: UTF -8
class Humano:
	def __init__(self,edad): #los guiones al piso indican el nivel de seguridad
		self.edad=edad 
		print "Hola soy un humano y tengo",edad,"años"
	def correr(self,km): #si los metodos no tienen la palabra reservada self se entiende que el metodo no pertenece a la clase
		print "Corro",km,"kilometros"
	def hablar(self,mensaje): #diferentes metodos pertenecientes a la clase Humano
		print "Les quiero decir que",mensaje

class IngSistemas(Humano): #Ya se hizo la herencia de Humano a IngSistemas
	def programar(self, lenguaje):#metodo propio de Ingsistemas
		print "Yo programo bien en",lenguaje

class Derecho(Humano):#metodo propio de Derecho
	def estudiocaso(self,caso):
		print "Estoy investigando en caso",caso

adriana=Derecho(20)
adriana.correr(10)
adriana.estudiocaso("Colmenares")

chris=Ingsistemas(20)
chris.hablar("Nada")
chris.programar("Python")